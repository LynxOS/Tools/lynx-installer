function userNext(){
    openLoading();
    saveUser(
        document.getElementById('username').value,
        document.getElementById('password').value,
        true);
    setRootPass(document.getElementById('password').value);
    document.getElementById("user-sidebar").classList.remove("active");
    drawKeyboard();
}

function keyboardNext(){
    openLoading();
    setKayboardLayout(document.getElementById('keyvariant').value);
    document.getElementById("keyboard-sidebar").classList.remove("active");
    drawDisks();
}

drawUser();