const homePath = nw.App.dataPath.concat('/../../../');
const execSync = require('child_process').spawnSync;
const exec = require('child_process').spawn;
const win = nw.Window.get();
const contentDiv = document.getElementById('installer-content');

// Define base install Options
const installOptions = {
    "dry_run": true,
    "audio": "pulseaudio",
    "debug": false,
    "harddrives": [],
    "bootloader": "systemd-bootctl",
    "hostname": "lynxos",
    "keyboard-layout": "us",
    "kernels": [
        "linux"
    ],
    "sys-encoding": "utf-8",
    "profile": "xorg",
    "!root-password": "lynxos",
    "!users": []
}

// Set windows Properties
win.setShowInTaskbar(true);
win.setPosition("center");