async function drawSidebarLogo(){
    const installerLogo = execSync('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'lynx-installer']).stdout.toString();
    document.getElementById('logo-installer').innerHTML = `<img src="file://${installerLogo}" class="img-fluid" />`;
    const userLogo = execSync('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'user-identity']).stdout.toString();
    document.getElementById('user-sidebar').innerHTML = `<img src="file://${userLogo}" class="img-fluid sidebar-menu" />`;
    const keyboardLogo = execSync('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'input-keyboard']).stdout.toString();
    document.getElementById('keyboard-sidebar').innerHTML = `<img src="file://${keyboardLogo}" class="img-fluid sidebar-menu" />`;
    const diskLogo = execSync('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'drive-harddisk']).stdout.toString();
    document.getElementById('disk-sidebar').innerHTML = `<img src="file://${diskLogo}" class="img-fluid sidebar-menu" />`;
}

drawSidebarLogo();