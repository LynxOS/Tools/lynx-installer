const keyboardData = [
    {
        "id": "us",
        "name": "USA",
        "variant": [
            {
                "id": "us",
                "name": "Default"
            },{
                "id": "chr",
                "name": "Cherokee"
            },{
                "id": "euro",
                "name": "With EuroSign on 5"
            },{
                "id": "intl",
                "name": "International (with dead keys)"
            },{
                "id": "alt-intl",
                "name": "Alternative international (former us_intl)"
            },{
                "id": "colemak",
                "name": "Colemak"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "dvorak-intl",
                "name": "Dvorak international"
            },{
                "id": "dvorak-l",
                "name": "Left handed Dvorak"
            },{
                "id": "dvorak-r",
                "name": "Right handed Dvorak"
            },{
                "id": "dvorak-classic",
                "name": "Classic Dvorak"
            },{
                "id": "dvp",
                "name": "Programmer Dvorak"
            },{
                "id": "rus",
                "name": "Russian phonetic"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "altgr-intl",
                "name": "International (AltGr dead keys)"
            },{
                "id": "olpc2",
                "name": "Group toggle on multiply/divide key"
            },{
                "id": "srp",
                "name": "Serbian"
            }
        ]
    },{
        "id": "ad",
        "name": "Andorra",
        "variant": [
            {
                "id": "ad",
                "name": "Default"
            }
        ]
    },{
        "id": "af",
        "name": "Afghanistan",
        "variant": [
            {
                "id": "af",
                "name": "Default"
            },{
                "id": "ps",
                "name": "Pashto"
            },{
                "id": "uz",
                "name": "Southern Uzbek"
            },{
                "id": "olpc-ps",
                "name": "OLPC Pashto"
            },{
                "id": "olpc-fa",
                "name": "OLPC Dari"
            },{
                "id": "olpc-uz",
                "name": "OLPC Southern Uzbek"
            }
        ]
    },{
        "id": "ara",
        "name": "Arabic",
        "variant": [
            {
                "id": "ara",
                "name": "Default"
            },{
                "id": "azerty",
                "name": "Azerty"
            },{
                "id": "azerty_digits",
                "name": "Azerty/Digits"
            },{
                "id": "digits",
                "name": "Digits"
            },{
                "id": "qwerty",
                "name": "QWERTY"
            },{
                "id": "qwerty_digits",
                "name": "QWERTY/Digits"
            },{
                "id": "buckwalter",
                "name": "Buckwalter"
            }
        ]
    },{
        "id": "al",
        "name": "Albania",
        "variant": [
            {
                "id": "al",
                "name": "Default"
            }
        ]
    },{
        "id": "am",
        "name": "Armenia",
        "variant": [
            {
                "id": "am",
                "name": "Default"
            },{
                "id": "phonetic",
                "name": "Phonetic"
            },{
                "id": "phonetic-alt",
                "name": "Alternative Phonetic"
            },{
                "id": "eastern",
                "name": "Eastern"
            },{
                "id": "western",
                "name": "Western"
            },{
                "id": "western-alt",
                "name": "Alternative Eastern"
            }
        ]
    },{
        "id": "az",
        "name": "Azerbaijan",
        "variant": [
            {
                "id": "az",
                "name": "Default"
            },{
                "id": "cyrillic",
                "name": "Cyrillic"
            }
        ]
    },{
        "id": "by",
        "name": "Belarus",
        "variant": [
            {
                "id": "by",
                "name": "Default"
            },{
                "id": "legacy",
                "name": "Legacy"
            },{
                "id": "latin",
                "name": "Latin"
            }
        ]
    },{
        "id": "be",
        "name": "Belgium",
        "variant": [
            {
                "id": "be",
                "name": "Default"
            },{
                "id": "oss",
                "name": "Alternative"
            },{
                "id": "oss_latin9",
                "name": "Alternative, latin-9 only"
            },{
                "id": "oss_sundeadkeys",
                "name": "Alternative, Sun dead keys"
            },{
                "id": "iso-alternate",
                "name": "ISO Alternate"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "wang",
                "name": "Wang model 724 azerty"
            }
        ]
    },{
        "id": "bd",
        "name": "Bangladesh",
        "variant": [
            {
                "id": "bd",
                "name": "Default"
            },{
                "id": "probhat",
                "name": "Probhat"
            }
        ]
    },{
        "id": "in",
        "name": "India",
        "variant": [
            {
                "id": "in",
                "name": "Default"
            },{
                "id": "ben",
                "name": "Bengali"
            },{
                "id": "ben_probhat",
                "name": "Bengali Probhat"
            },{
                "id": "guj",
                "name": "Gujarati"
            },{
                "id": "guru",
                "name": "Gurmukhi"
            },{
                "id": "jhelum",
                "name": "Gurmukhi Jhelum"
            },{
                "id": "kan",
                "name": "Kannada"
            },{
                "id": "mal",
                "name": "Malayalam"
            },{
                "id": "mal_lalitha",
                "name": "Malayalam Lalitha"
            },{
                "id": "ori",
                "name": "Oriya"
            },{
                "id": "tam_unicode",
                "name": "Tamil Unicode"
            },{
                "id": "tam_keyboard_with_numerals",
                "name": "Tamil Keyboard with Numerals"
            },{
                "id": "tam_TAB",
                "name": "Tamil TAB Typewriter"
            },{
                "id": "tam_TSCII",
                "name": "Tamil TSCII Typewriter"
            },{
                "id": "tam",
                "name": "Tamil"
            },{
                "id": "tel",
                "name": "Telugu"
            },{
                "id": "urd-phonetic",
                "name": "Urdu, Phonetic"
            },{
                "id": "urd-phonetic3",
                "name": "Urdu, Alternative phonetic"
            },{
                "id": "urd-winkeys",
                "name": "Urdu, Winkeys"
            },{
                "id": "bolnagri",
                "name": "Hindi Bolnagri"
            },{
                "id": "hin-wx",
                "name": "Hindi Wx"
            }
        ]
    },{
        "id": "ba",
        "name": "Bosnia and Herzegovina",
        "variant": [
            {
                "id": "ba",
                "name": "Default"
            },{
                "id": "alternatequotes",
                "name": "Use guillemets for quotes"
            },{
                "id": "unicode",
                "name": "Use Bosnian digraphs"
            },{
                "id": "unicodeus",
                "name": "US keyboard with Bosnian digraphs"
            },{
                "id": "us",
                "name": "US keyboard with Bosnian letters"
            }
        ]
    },{
        "id": "br",
        "name": "Brazil",
        "variant": [
            {
                "id": "br",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "nativo",
                "name": "Nativo"
            },{
                "id": "nativo-us",
                "name": "Nativo for USA keyboards"
            },{
                "id": "nativo-epo",
                "name": "Nativo for Esperanto"
            }
        ]
    },{
        "id": "bg",
        "name": "Bulgaria",
        "variant": [
            {
                "id": "bg",
                "name": "Default"
            },{
                "id": "phonetic",
                "name": "Traditional phonetic"
            },{
                "id": "bas_phonetic",
                "name": "New phonetic"
            }
        ]
    },{
        "id": "ma",
        "name": "Morocco",
        "variant": [
            {
                "id": "ma",
                "name": "Default"
            },{
                "id": "french",
                "name": "French"
            },{
                "id": "tifinagh",
                "name": "Tifinagh"
            },{
                "id": "tifinagh-alt",
                "name": "Tifinagh Alternative"
            },{
                "id": "tifinagh-alt-phonetic",
                "name": "Tifinagh Alternative Phonetic"
            },{
                "id": "tifinagh-extended",
                "name": "Tifinagh Extended"
            },{
                "id": "tifinagh-phonetic",
                "name": "Tifinagh Phonetic"
            },{
                "id": "tifinagh-extended-phonetic",
                "name": "Tifinagh Extended Phonetic"
            }
        ]
    },{
        "id": "mm",
        "name": "Myanmar",
        "variant": [
            {
                "id": "mm",
                "name": "Default"
            }
        ]
    },{
        "id": "ca",
        "name": "Canada",
        "variant": [
            {
                "id": "ca",
                "name": "Default"
            },{
                "id": "fr-dvorak",
                "name": "French Dvorak"
            },{
                "id": "fr-legacy",
                "name": "French (legacy)"
            },{
                "id": "multix",
                "name": "Multilingual"
            },{
                "id": "multi",
                "name": "Multilingual, first part"
            },{
                "id": "multi-2gr",
                "name": "Multilingual, second part"
            },{
                "id": "ike",
                "name": "Inuktitut"
            },{
                "id": "shs",
                "name": "Secwepemctsin"
            },{
                "id": "kut",
                "name": "Ktunaxa"
            },{
                "id": "eng",
                "name": "English"
            }
        ]
    },{
        "id": "cd",
        "name": "Congo, Democratic Republic of the",
        "variant": [
            {
                "id": "cd",
                "name": "Default"
            }
        ]
    },{
        "id": "cn",
        "name": "China",
        "variant": [
            {
                "id": "cn",
                "name": "Default"
            },{
                "id": "tib",
                "name": "Tibetan"
            },{
                "id": "tib",
                "name": "Tibetan (with ASCII numerals)"
            }
        ]
    },{
        "id": "hr",
        "name": "Croatia",
        "variant": [
            {
                "id": "hr",
                "name": "Default"
            },{
                "id": "alternatequotes",
                "name": "Use guillemets for quotes"
            },{
                "id": "unicode",
                "name": "Use Croatian digraphs"
            },{
                "id": "unicodeus",
                "name": "US keyboard with Croatian digraphs"
            },{
                "id": "us",
                "name": "US keyboard with Croatian letters"
            }
        ]
    },{
        "id": "cz",
        "name": "Czechia",
        "variant": [
            {
                "id": "cz",
                "name": "Default"
            },{
                "id": "bksl",
                "name": "With &lt;\|&gt; key"
            },{
                "id": "qwerty",
                "name": "qwerty"
            },{
                "id": "qwerty_bksl",
                "name": "qwerty, extended Backslash"
            },{
                "id": "ucw",
                "name": "UCW layout (accented letters only)"
            },{
                "id": "dvorak-ucw",
                "name": " US Dvorak with CZ UCW support"
            }
        ]
    },{
        "id": "dk",
        "name": "Denmark",
        "variant": [
            {
                "id": "dk",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "mac_nodeadkeys",
                "name": "Macintosh, eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            }
        ]
    },{
        "id": "nl",
        "name": "Netherlands",
        "variant": [
            {
                "id": "nl",
                "name": "Default"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "std",
                "name": "Standard"
            }
        ]
    },{
        "id": "bt",
        "name": "Bhutan",
        "variant": [
            {
                "id": "bt",
                "name": "Default"
            }
        ]
    },{
        "id": "ee",
        "name": "Estonia",
        "variant": [
            {
                "id": "ee",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "us",
                "name": "US keyboard with Estonian letters"
            }
        ]
    },{
        "id": "ir",
        "name": "Iran",
        "variant": [
            {
                "id": "ir",
                "name": "Default"
            },{
                "id": "pes_keypad",
                "name": "Persian, with Persian Keypad"
            },{
                "id": "ku",
                "name": "Kurdish, Latin Q"
            },{
                "id": "ku_f",
                "name": "Kurdish, (F)"
            },{
                "id": "ku_alt",
                "name": "Kurdish, Latin Alt-Q"
            },{
                "id": "ku_ara",
                "name": "Kurdish, Arabic-Latin"
            }
        ]
    },{
        "id": "iq",
        "name": "Iraq",
        "variant": [
            {
                "id": "iq",
                "name": "Default"
            },{
                "id": "ku",
                "name": "Kurdish, Latin Q"
            },{
                "id": "ku_f",
                "name": "Kurdish, (F)"
            },{
                "id": "ku_alt",
                "name": "Kurdish, Latin Alt-Q"
            },{
                "id": "ku_ara",
                "name": "Kurdish, Arabic-Latin"
            }
        ]
    },{
        "id": "fo",
        "name": "Faroe Islands",
        "variant": [
            {
                "id": "fo",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            }
        ]
    },{
        "id": "fi",
        "name": "Finland",
        "variant": [
            {
                "id": "fo",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "smi",
                "name": "Northern Saami"
            },{
                "id": "classic",
                "name": "Classic"
            },{
                "id": "mac",
                "name": "Macintosh"
            }
        ]
    },{
        "id": "fr",
        "name": "France",
        "variant": [
            {
                "id": "fr",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "oss",
                "name": "Alternative"
            },{
                "id": "oss_latin9",
                "name": "Alternative, latin-9 only"
            },{
                "id": "oss_nodeadkeys",
                "name": "Alternative, eliminate dead keys"
            },{
                "id": "oss_sundeadkeys",
                "name": "Alternative, Sun dead keys"
            },{
                "id": "latin9",
                "name": "(Legacy) Alternative"
            },{
                "id": "latin9_nodeadkeys",
                "name": "(Legacy) Alternative, eliminate dead keys"
            },{
                "id": "latin9_sundeadkeys",
                "name": "(Legacy) Alternative, Sun dead keys"
            },{
                "id": "bepo",
                "name": "Bepo, ergonomic, Dvorak way"
            },{
                "id": "bepo_latin9",
                "name": "Bepo, ergonomic, Dvorak way, latin-9 only"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "bre",
                "name": "Breton"
            },{
                "id": "oci",
                "name": "Occitan"
            },{
                "id": "geo",
                "name": "Georgian AZERTY Tskapo"
            }
        ]
    },{
        "id": "gh",
        "name": "Ghana",
        "variant": [
            {
                "id": "gh",
                "name": "Default"
            },{
                "id": "generic",
                "name": "Multilingual"
            },{
                "id": "akan",
                "name": "Akan"
            },{
                "id": "ewe",
                "name": "Ewe"
            },{
                "id": "fula",
                "name": "Fula"
            },{
                "id": "ga",
                "name": "Ga"
            },{
                "id": "hausa",
                "name": "Hausa"
            }
        ]
    },{
        "id": "gn",
        "name": "Guinea",
        "variant": [
            {
                "id": "gn",
                "name": "Default"
            }
        ]
    },{
        "id": "ge",
        "name": "Georgia",
        "variant": [
            {
                "id": "ge",
                "name": "Default"
            },{
                "id": "ergonomic",
                "name": "Ergonomic"
            },{
                "id": "mess",
                "name": "MESS"
            },{
                "id": "ru",
                "name": "Russian"
            },{
                "id": "os",
                "name": "Ossetian"
            }
        ]
    },{
        "id": "de",
        "name": "Germany",
        "variant": [
            {
                "id": "de",
                "name": "Default"
            },{
                "id": "deadacute",
                "name": "Dead acute"
            },{
                "id": "deadgraveacute",
                "name": "Dead grave acute"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "ro",
                "name": "Romanian keyboard with German letters"
            },{
                "id": "ro_nodeadkeys",
                "name": "Romanian keyboard with German letters, eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "neo",
                "name": "Neo 2"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "mac_nodeadkeys",
                "name": "Macintosh, eliminate dead keys"
            },{
                "id": "dsb",
                "name": "Lower Sorbian"
            },{
                "id": "dsb_qwertz",
                "name": "Lower Sorbian (qwertz)"
            },{
                "id": "qwertz",
                "name": "qwertz"
            }
        ]
    },{
        "id": "gr",
        "name": "Greece",
        "variant": [
            {
                "id": "gr",
                "name": "Default"
            },{
                "id": "simple",
                "name": "Simple"
            },{
                "id": "extended",
                "name": "Extended"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "polytonic",
                "name": "Polytonic"
            }
        ]
    },{
        "id": "hu",
        "name": "Hungary",
        "variant": [
            {
                "id": "hu",
                "name": "Default"
            },{
                "id": "standard",
                "name": "Standard"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "qwerty",
                "name": "qwerty"
            },{
                "id": "101_qwertz_comma_dead",
                "name": "101/qwertz/comma/Dead keys"
            },{
                "id": "101_qwertz_comma_nodead",
                "name": "101/qwertz/comma/Eliminate dead keys"
            },{
                "id": "101_qwertz_dot_dead",
                "name": "101/qwertz/dot/Dead keys"
            },{
                "id": "101_qwertz_dot_nodead",
                "name": "101/qwertz/dot/Eliminate dead keys"
            },{
                "id": "101_qwerty_comma_dead",
                "name": "101/qwerty/comma/Dead keys"
            },{
                "id": "101_qwerty_comma_nodead",
                "name": "101/qwerty/comma/Eliminate dead keys"
            },{
                "id": "101_qwerty_dot_dead",
                "name": "101/qwerty/dot/Dead keys"
            },{
                "id": "101_qwerty_dot_nodead",
                "name": "101/qwerty/dot/Eliminate dead keys"
            },{
                "id": "102_qwertz_comma_dead",
                "name": "102/qwertz/comma/Dead keys"
            },{
                "id": "102_qwertz_comma_nodead",
                "name": "102/qwertz/comma/Eliminate dead keys"
            },{
                "id": "102_qwertz_dot_dead",
                "name": "102/qwertz/dot/Dead keys"
            },{
                "id": "102_qwertz_dot_nodead",
                "name": "102/qwertz/dot/Eliminate dead keys"
            },{
                "id": "102_qwerty_comma_dead",
                "name": "102/qwerty/comma/Dead keys"
            },{
                "id": "102_qwerty_comma_nodead",
                "name": "102/qwerty/comma/Eliminate dead keys"
            },{
                "id": "102_qwerty_dot_dead",
                "name": "102/qwerty/dot/Dead keys"
            },{
                "id": "102_qwerty_dot_nodead",
                "name": "102/qwerty/dot/Eliminate dead keys"
            }
        ]
    },{
        "id": "is",
        "name": "Iceland",
        "variant": [
            {
                "id": "is",
                "name": "Default"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            }
        ]
    },{
        "id": "il",
        "name": "Israel",
        "variant": [
            {
                "id": "il",
                "name": "Default"
            },{
                "id": "lyx",
                "name": "lyx"
            },{
                "id": "phonetic",
                "name": "Phonetic"
            },{
                "id": "biblical",
                "name": "Biblical Hebrew (Tiro)"
            }
        ]
    },{
        "id": "it",
        "name": "Italy",
        "variant": [
            {
                "id": "it",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "us",
                "name": "US keyboard with Italian letters"
            },{
                "id": "geo",
                "name": "Georgian"
            }
        ]
    },{
        "id": "jp",
        "name": "Japan",
        "variant": [
            {
                "id": "jp",
                "name": "Default"
            },{
                "id": "kana",
                "name": "Kana"
            },{
                "id": "OADG109A",
                "name": "OADG 109A"
            },{
                "id": "mac",
                "name": "Macintosh"
            }
        ]
    },{
        "id": "kg",
        "name": "Kyrgyzstan",
        "variant": [
            {
                "id": "kg",
                "name": "Default"
            },{
                "id": "phonetic",
                "name": "Phonetic"
            }
        ]
    },{
        "id": "kh",
        "name": "Cambodia",
        "variant": [
            {
                "id": "kh",
                "name": "Default"
            }
        ]
    },{
        "id": "kz",
        "name": "Kazakhstan",
        "variant": [
            {
                "id": "kz",
                "name": "Default"
            },{
                "id": "ruskaz",
                "name": "Russian with Kazakh"
            },{
                "id": "kazrus",
                "name": "Kazakh with Russian"
            }
        ]
    },{
        "id": "la",
        "name": "Laos",
        "variant": [
            {
                "id": "la",
                "name": "Default"
            },{
                "id": "basic",
                "name": "Laos"
            },{
                "id": "stea",
                "name": "Laos - STEA (proposed standard layout)"
            }
        ]
    },{
        "id": "latam",
        "name": "Latin American",
        "variant": [
            {
                "id": "latam",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "deadtilde",
                "name": "Include dead tilde"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            }
        ]
    },{
        "id": "lt",
        "name": "Lithuania",
        "variant": [
            {
                "id": "lt",
                "name": "Default"
            },{
                "id": "std",
                "name": "Standard"
            },{
                "id": "us",
                "name": "US keyboard with Lithuanian letters"
            },{
                "id": "ibm",
                "name": "IBM (LST 1205-92)"
            },{
                "id": "lekp",
                "name": "LEKP"
            },{
                "id": "lekpa",
                "name": "LEKPa"
            }
        ]
    },{
        "id": "lv",
        "name": "Latvia",
        "variant": [
            {
                "id": "lv",
                "name": "Default"
            },{
                "id": "apostrophe",
                "name": "Apostrophe (') variant"
            },{
                "id": "tilde",
                "name": "Tilde (~) variant"
            },{
                "id": "fkey",
                "name": "F-letter (F) variant"
            }
        ]
    },{
        "id": "mao",
        "name": "Maori",
        "variant": [
            {
                "id": "mao",
                "name": "Default"
            }
        ]
    },{
        "id": "me",
        "name": "Montenegro",
        "variant": [
            {
                "id": "me",
                "name": "Default"
            },{
                "id": "cyrillic",
                "name": "Cyrillic"
            },{
                "id": "cyrillicyz",
                "name": "Cyrillic, Z and ZHE swapped"
            },{
                "id": "latinunicode",
                "name": "Latin unicode"
            },{
                "id": "latinyz",
                "name": "Latin qwerty"
            },{
                "id": "latinunicodeyz",
                "name": "Latin unicode qwerty"
            },{
                "id": "cyrillicalternatequotes",
                "name": "Cyrillic with guillemets"
            },{
                "id": "latinalternatequotes",
                "name": "Latin with guillemets"
            }
        ]
    },{
        "id": "mk",
        "name": "Macedonia",
        "variant": [
            {
                "id": "mk",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            }
        ]
    },{
        "id": "mt",
        "name": "Malta",
        "variant": [
            {
                "id": "mt",
                "name": "Default"
            },{
                "id": "us",
                "name": "Maltese keyboard with US layout"
            }
        ]
    },{
        "id": "mn",
        "name": "Mongolia",
        "variant": [
            {
                "id": "mn",
                "name": "Default"
            }
        ]
    },{
        "id": "no",
        "name": "Norway",
        "variant": [
            {
                "id": "no",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "smi",
                "name": "Northern Saami"
            },{
                "id": "smi_nodeadkeys",
                "name": "Northern Saami, eliminate dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "mac_nodeadkeys",
                "name": "Macintosh, eliminate dead keys"
            }
        ]
    },{
        "id": "pl",
        "name": "Poland",
        "variant": [
            {
                "id": "pl",
                "name": "Default"
            },{
                "id": "qwertz",
                "name": "qwertz"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "dvorak_quotes",
                "name": "Dvorak, Polish quotes on quotemark key"
            },{
                "id": "dvorak_altquotes",
                "name": "Dvorak, Polish quotes on key 1"
            },{
                "id": "csb",
                "name": "Kashubian"
            },{
                "id": "ru_phonetic_dvorak",
                "name": "Russian phonetic Dvorak"
            },{
                "id": "dvp",
                "name": "Programmer Dvorak"
            }
        ]
    },{
        "id": "pt",
        "name": "Portugal",
        "variant": [
            {
                "id": "pt",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "mac_nodeadkeys",
                "name": "Macintosh, eliminate dead keys"
            },{
                "id": "mac_sundeadkeys",
                "name": "Macintosh, Sun dead keys"
            },{
                "id": "nativo",
                "name": "Nativo"
            },{
                "id": "nativo-us",
                "name": "Nativo for USA keyboards"
            },{
                "id": "nativo-epo",
                "name": "Nativo for Esperanto9"
            }
        ]
    },{
        "id": "ro",
        "name": "Romania",
        "variant": [
            {
                "id": "ro",
                "name": "Default"
            },{
                "id": "cedilla",
                "name": "Cedilla"
            },{
                "id": "std",
                "name": "Standard"
            },{
                "id": "std_cedilla",
                "name": "Standard (Cedilla)"
            },{
                "id": "winkeys",
                "name": "Winkeys"
            },{
                "id": "crh_f",
                "name": "Crimean Tatar (Turkish F)"
            },{
                "id": "crh_alt",
                "name": "Crimean Tatar (Turkish Alt-Q)"
            },{
                "id": "crh_dobruca1",
                "name": "Crimean Tatar (Dobruca-1 Q)"
            },{
                "id": "crh_dobruca2",
                "name": "Crimean Tatar (Dobruca-2 Q)"
            }
        ]
    },{
        "id": "ru",
        "name": "Russia",
        "variant": [
            {
                "id": "ru",
                "name": "Default"
            },{
                "id": "phonetic",
                "name": "Phonetic"
            },{
                "id": "phonetic_winkeys",
                "name": "Phonetic Winkeys"
            },{
                "id": "typewriter",
                "name": "Typewriter"
            },{
                "id": "legacy",
                "name": "Legacy"
            },{
                "id": "typewriter-legacy",
                "name": "Typewriter, legacy"
            },{
                "id": "tt",
                "name": "Tatar"
            },{
                "id": "os_legacy",
                "name": "Ossetian, legacy"
            },{
                "id": "os_winkeys",
                "name": "Ossetian, Winkeys"
            },{
                "id": "cv",
                "name": "Chuvash"
            },{
                "id": "cv_latin",
                "name": "Chuvash Latin"
            },{
                "id": "udm",
                "name": "Udmurt"
            },{
                "id": "kom",
                "name": "Komi"
            },{
                "id": "sah",
                "name": "Yakut"
            },{
                "id": "xal",
                "name": "Kalmyk"
            },{
                "id": "dos",
                "name": "DOS"
            },{
                "id": "srp",
                "name": "Serbian"
            },{
                "id": "bak",
                "name": "Bashkirian"
            }
        ]
    },{
        "id": "rs",
        "name": "Serbia",
        "variant": [
            {
                "id": "rs",
                "name": "Default"
            },{
                "id": "yz",
                "name": "Z and ZHE swapped"
            },{
                "id": "latin",
                "name": "Latin"
            },{
                "id": "latinunicode",
                "name": "Latin Unicode"
            },{
                "id": "latinyz",
                "name": "Latin qwerty"
            },{
                "id": "latinunicodeyz",
                "name": "Latin Unicode qwerty"
            },{
                "id": "alternatequotes",
                "name": "With guillemets"
            },{
                "id": "latinalternatequotes",
                "name": "Latin with guillemets"
            }
        ]
    },{
        "id": "si",
        "name": "Slovenia",
        "variant": [
            {
                "id": "si",
                "name": "Default"
            },{
                "id": "alternatequotes",
                "name": "Use guillemets for quotes"
            },{
                "id": "us",
                "name": "US keyboard with Slovenian letters"
            }
        ]
    },{
        "id": "sk",
        "name": "Slovakia",
        "variant": [
            {
                "id": "sk",
                "name": "Default"
            },{
                "id": "bksl",
                "name": "Extended Backslash"
            },{
                "id": "qwerty",
                "name": "Default"
            },{
                "id": "qwerty_bksl",
                "name": "qwerty, extended Backslash"
            }
        ]
    },{
        "id": "es",
        "name": "Spain",
        "variant": [
            {
                "id": "es",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "deadtilde",
                "name": "Include dead tilde"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "ast",
                "name": "Asturian variant with bottom-dot H and bottom-dot L"
            },{
                "id": "cat",
                "name": "Catalan variant with middle-dot L"
            },{
                "id": "mac",
                "name": "Macintosh"
            }
        ]
    },{
        "id": "se",
        "name": "Sweden",
        "variant": [
            {
                "id": "se",
                "name": "Default"
            },{
                "id": "nodeadkeys",
                "name": "Eliminate dead keys"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "rus",
                "name": "Russian phonetic"
            },{
                "id": "rus_nodeadkeys",
                "name": "Russian phonetic, eliminate dead keys"
            },{
                "id": "smi",
                "name": "Northern Saami"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "svdvorak",
                "name": "Svdvorak"
            }
        ]
    },{
        "id": "ch",
        "name": "Switzerland",
        "variant": [
            {
                "id": "ch",
                "name": "Default"
            },{
                "id": "legacy",
                "name": "Legacy"
            },{
                "id": "de_nodeadkeys",
                "name": "German, eliminate dead keys"
            },{
                "id": "de_sundeadkeys",
                "name": "German, Sun dead keys"
            },{
                "id": "fr",
                "name": "French"
            },{
                "id": "fr_nodeadkeys",
                "name": "French, eliminate dead keys"
            },{
                "id": "fr_sundeadkeys",
                "name": "French, Sun dead keys"
            },{
                "id": "fr_mac",
                "name": "French (Macintosh)"
            },{
                "id": "de_mac",
                "name": "German (Macintosh)"
            }
        ]
    },{
        "id": "sy",
        "name": "Syria",
        "variant": [
            {
                "id": "sy",
                "name": "Default"
            },{
                "id": "syc",
                "name": "Syriac"
            },{
                "id": "syc_phonetic",
                "name": "Syriac phonetic"
            },{
                "id": "ku",
                "name": "Kurdish, Latin Q"
            },{
                "id": "ku_f",
                "name": "Kurdish, (F)"
            },{
                "id": "ku_alt",
                "name": "Kurdish, Latin Alt-Q"
            }
        ]
    },{
        "id": "tj",
        "name": "Tajikistan",
        "variant": [
            {
                "id": "tj",
                "name": "Default"
            },{
                "id": "legacy",
                "name": "Legacy"
            }
        ]
    },{
        "id": "lk",
        "name": "Sri Lanka",
        "variant": [
            {
                "id": "lk",
                "name": "Default"
            },{
                "id": "tam_unicode",
                "name": "Tamil Unicode"
            },{
                "id": "tam_TAB",
                "name": "Tamil TAB Typewriter"
            }
        ]
    },{
        "id": "th",
        "name": "Thailand",
        "variant": [
            {
                "id": "th",
                "name": "Default"
            },{
                "id": "tis",
                "name": "TIS-820.2538"
            },{
                "id": "pat",
                "name": "Pattachote"
            }
        ]
    },{
        "id": "tr",
        "name": "Turkey",
        "variant": [
            {
                "id": "tr",
                "name": "Default"
            },{
                "id": "f",
                "name": "(F)"
            },{
                "id": "alt",
                "name": "Alt-Q"
            },{
                "id": "sundeadkeys",
                "name": "Sun dead keys"
            },{
                "id": "ku",
                "name": "Kurdish, Latin Q"
            },{
                "id": "ku_f",
                "name": "Kurdish, (F)"
            },{
                "id": "ku_alt",
                "name": "Kurdish, Latin Alt-Q"
            },{
                "id": "intl",
                "name": "International (with dead keys)"
            },{
                "id": "crh",
                "name": "Crimean Tatar (Turkish Q)"
            },{
                "id": "crh_f",
                "name": "Crimean Tatar (Turkish F)"
            },{
                "id": "crh_alt",
                "name": "Crimean Tatar (Turkish Alt-Q)"
            }
        ]
    },{
        "id": "tw",
        "name": "Taiwan",
        "variant": [
            {
                "id": "tw",
                "name": "Default"
            },{
                "id": "indigenous",
                "name": "Indigenous"
            },{
                "id": "saisiyat",
                "name": "Saisiyat"
            }
        ]
    },{
        "id": "ua",
        "name": "Ukraine",
        "variant": [
            {
                "id": "ua",
                "name": "Default"
            },{
                "id": "phonetic",
                "name": "Phonetic"
            },{
                "id": "typewriter",
                "name": "Typewriter"
            },{
                "id": "winkeys",
                "name": "Winkeys"
            },{
                "id": "legacy",
                "name": "Legacy"
            },{
                "id": "rstu",
                "name": "Standard RSTU"
            },{
                "id": "rstu_ru",
                "name": "Standard RSTU on Russian layout"
            },{
                "id": "homophonic",
                "name": "Homophonic"
            },{
                "id": "crh",
                "name": "Crimean Tatar (Turkish Q)"
            },{
                "id": "crh_f",
                "name": "Crimean Tatar (Turkish F)"
            },{
                "id": "crh_alt",
                "name": "Crimean Tatar (Turkish Alt-Q)"
            }
        ]
    },{
        "id": "gb",
        "name": "United Kingdom",
        "variant": [
            {
                "id": "gb",
                "name": "Default"
            },{
                "id": "extd",
                "name": "Extended - Winkeys"
            },{
                "id": "intl",
                "name": "International (with dead keys)"
            },{
                "id": "dvorak",
                "name": "Dvorak"
            },{
                "id": "dvorakukp",
                "name": "Dvorak (UK Punctuation)"
            },{
                "id": "mac",
                "name": "Macintosh"
            },{
                "id": "colemak",
                "name": "Colemak"
            }
        ]
    },{
        "id": "uz",
        "name": "Uzbekistan",
        "variant": [
            {
                "id": "uz",
                "name": "Default"
            },{
                "id": "latin",
                "name": "Latin"
            },{
                "id": "crh",
                "name": "Crimean Tatar (Turkish Q)"
            },{
                "id": "crh_f",
                "name": "Crimean Tatar (Turkish F)"
            },{
                "id": "crh_alt",
                "name": "Crimean Tatar (Turkish Alt-Q)"
            }
        ]
    },{
        "id": "vn",
        "name": "Vietnam",
        "variant": [
            {
                "id": "vm",
                "name": "Default"
            }
        ]
    },{
        "id": "kr",
        "name": "Korea, Republic of",
        "variant": [
            {
                "id": "kr",
                "name": "Default"
            },{
                "id": "kr104",
                "name": "101/104 key Compatible"
            }
        ]
    },{
        "id": "nec_vndr/jp",
        "name": "Japan (PC-98xx Series)",
        "variant": [
            {
                "id": "nec_vndr/jp",
                "name": "Default"
            }
        ]
    },{
        "id": "ie",
        "name": "Ireland",
        "variant": [
            {
                "id": "ie",
                "name": "Default"
            },{
                "id": "CloGaelach",
                "name": "CloGaelach"
            },{
                "id": "UnicodeExpert",
                "name": "UnicodeExpert"
            },{
                "id": "ogham",
                "name": "Ogham"
            },{
                "id": "ogham_is434",
                "name": "Ogham IS434"
            }
        ]
    },{
        "id": "pk",
        "name": "Pakistan",
        "variant": [
            {
                "id": "pk",
                "name": "Default"
            },{
                "id": "urd-crulp",
                "name": "CRULP"
            },{
                "id": "urd-nla",
                "name": "Default"
            },{
                "id": "ara",
                "name": "Arabic"
            }
        ]
    },{
        "id": "mv",
        "name": "Maldives",
        "variant": [
            {
                "id": "mv",
                "name": "Default"
            }
        ]
    },{
        "id": "za",
        "name": "South Africa",
        "variant": [
            {
                "id": "za",
                "name": "Default"
            }
        ]
    },{
        "id": "epo",
        "name": "Esperanto",
        "variant": [
            {
                "id": "epo",
                "name": "Default"
            },{
                "id": "legacy",
                "name": "displaced semicolon and quote (obsolete)"
            }
        ]
    },{
        "id": "np",
        "name": "Nepal",
        "variant": [
            {
                "id": "np",
                "name": "Default"
            }
        ]
    },{
        "id": "ng",
        "name": "Nigeria",
        "variant": [
            {
                "id": "ng",
                "name": "Default"
            },{
                "id": "igbo",
                "name": "Igbo"
            },{
                "id": "yoruba",
                "name": "Yoruba"
            },{
                "id": "hausa",
                "name": "Hausa"
            }
        ]
    },{
        "id": "et",
        "name": "Ethiopia",
        "variant": [
            {
                "id": "et",
                "name": "Default"
            }
        ]
    },{
        "id": "sn",
        "name": "Senegal",
        "variant": [
            {
                "id": "sn",
                "name": "Default"
            }
        ]
    },{
        "id": "brai",
        "name": "Braille",
        "variant": [
            {
                "id": "brai",
                "name": "Default"
            },{
                "id": "left_hand",
                "name": "Left hand"
            },{
                "id": "right_hand",
                "name": "Right hand"
            }
        ]
    },{
        "id": "tm",
        "name": "Turkmenistan",
        "variant": [
            {
                "id": "tm",
                "name": "Default"
            },{
                "id": "alt",
                "name": "Alt-Q"
            }
        ]
    },{
        "id": "ml",
        "name": "Mali",
        "variant": [
            {
                "id": "ml",
                "name": "Default"
            },{
                "id": "fr-oss",
                "name": "Français (France Alternative)"
            },{
                "id": "us-mac",
                "name": "English (USA Macintosh)"
            },{
                "id": "us-intl",
                "name": "English (USA International)"
            }
        ]
    },{
        "id": "tz",
        "name": "Tanzania",
        "variant": [
            {
                "id": "tz",
                "name": "Default"
            }
        ]
    }
]