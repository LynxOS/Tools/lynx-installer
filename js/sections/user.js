async function drawUser(){
    document.getElementById("user-sidebar").classList.remove("disabled");
    contentDiv.innerHTML = `
    <div class="text-center p-5">
        <h1>LynxOS "Papilio"</h1>
        <h2 style="margin-bottom: 40px;">Installer</h2>
        <img src="file://${homePath}.face" class="img-fluid lynx-profile-picture" />
        <div class="p-5">
            <div class="input-group mb-3" id="user-data">
                <span class="input-group-text" id="basic-addon1">User</span>
                <input type="text" class="form-control" placeholder="lynx" id="username" aria-label="User" aria-describedby="basic-addon1">
            </div>
            <div class="input-group mb-3" id="user-data">
                <span class="input-group-text" id="basic-addon1">Pass</span>
                <input type="password" class="form-control" placeholder="mypassword" id="password" aria-label="User" aria-describedby="basic-addon1">
            </div>
        </div>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button class="btn btn-primary" onclick="userNext();" type="button">&nbsp; &nbsp; -> &nbsp; &nbsp;</button>
        </div>
    </div>
    `
    document.getElementById("user-sidebar").classList.add("active");
    closeLoading();
}

function saveUser(username, password, isRoot){
    let newUser = {
        "username": username,
        "!password": password,
        "sudo": isRoot
    }
    installOptions['!users'].push(newUser);
}