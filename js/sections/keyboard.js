async function drawKeyboard(){
    document.getElementById("keyboard-sidebar").classList.remove("disabled");
    contentDiv.innerHTML =`
    <div class="text-center p-5">
        <h1 style="margin-bottom: 60px;">Keyboard</h1>
        <label for="layoutsInput" class="form-label">Select Layout</label>
        <input
            class="form-control"
            list="layouts"
            onchange="drawKeyboardVariants()"
            id="layoutsInput"
            placeholder="Type to search..." />
        ${drawKeyboardLayouts()}
        <div style="margin-top: 60px;" id="variantDiv"></div>
    </div>
    <div class="text-center p-5" style="padding-top: 0px !important;" id="keyboard-variants"></div>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <button class="btn btn-primary" onclick="keyboardNext();" type="button">&nbsp; &nbsp; -> &nbsp; &nbsp;</button>
    </div>
    `
    document.getElementById("keyboard-sidebar").classList.add("active");
    closeLoading();
}

function drawKeyboardLayouts(){
    let html = `<datalist id="layouts">`;
    for (layout of keyboardData){
        html = html.concat(`<option value="${layout.name}">`);
    }
    return html.concat(`</datalist>`);
}

function drawKeyboardVariants(){
    openLoading();
    const layout = document.getElementById('layoutsInput').value;
    const data = keyboardData.find(keyboardLayout => keyboardLayout.name == layout);
    let html = `<select class="form-select text-center" id="keyvariant" multiple aria-label="multiple select example">`;
    for (variant of data.variant){
        html = html.concat(`<option value="${variant.id}">${variant.name}</option>`);
    }
    document.getElementById("keyboard-variants").innerHTML = html.concat('</select>');
    closeLoading();
}

function setKayboardLayout(layout){
    installOptions['keyboard-layout'] = layout;
}