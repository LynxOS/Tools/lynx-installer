const systemInformation = require('systeminformation');
let disks;
let ram;
systemInformation.diskLayout().then(data => {disks = data});
systemInformation.diskLayout().then(data => {
    ram = (data.total / 1073741824).toFixed(2)
});

async function drawDisks(){
    document.getElementById("disk-sidebar").classList.remove("disabled");
    contentDiv.innerHTML =`
    <div class="p-5">
        <h1 class="text-center" style="margin-bottom: 10px;">Disk</h1>
        
        <select class="form-select text-center" aria-label="Select Disk">
            ${drawDiskData()}
        </select>

        ${drawSwapData()}

        <div class="progress" id="disk-graph"></div>

        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button class="btn btn-primary" onclick="keyboardNext();" type="button">&nbsp; &nbsp; -> &nbsp; &nbsp;</button>
        </div>
    </div>
    `
    document.getElementById("disk-sidebar").classList.add("active");
    setPreDiskData();
    closeLoading();
}

function drawDiskData(){
    let html = '';
    for (disk of disks){
        html = html.concat(`<option value="${disk.name}">${disk.name} | ${disk.type} | ${disk.device} | ${disk.vendor} | ${disk.size}</option>`);
    }
    return html;
}

function drawSwapData(){
    let html = `<div class="container-fluid swap-container">
                    <h2 class="text-center swap-title">Swap</h2>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-check" style="padding-left:80px; margin-top: -40px;">
                                <input class="form-check-input" type="checkbox" value="" id="useSwapCheck">
                                <label class="form-check-label" for="useSwapCheck">
                                    Swap
                                </label>
                            </div>
                            <div class="input-group mb-3">
                                <input type="number" class="form-control" placeholder="8" aria-label="8" aria-describedby="swap-gb-desc">
                                <span class="input-group-text" id="swap-gb-desc">GB</span>
                            </div>
                        </div>
                    </div>
                </div>`;

    return html;
}

function setPreDiskData(){
    if (ram < 6){
        document.getElementById('useSwapCheck').checked = true;
    }
}